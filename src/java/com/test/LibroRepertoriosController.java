/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.test;

import com.estructuras.*;
import com.google.gson.JsonObject;
import com.utils.ConectorMySql;
import com.utils.Constantes;
import com.utils.Funciones;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Ivan
 */
public class LibroRepertoriosController {

    public static ArrayList<CaratulaRepertorio> getCaratulasLibroRepertorios() throws SQLException, ParseException, ClassNotFoundException {

        Date date = new Date();

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.HOUR_OF_DAY, 0);

        date = calendar.getTime();
        long date_long = date.getTime();
        System.out.println(Constantes.UN_DIA_MILLIS);

        ArrayList<CaratulaRepertorio> caratulas = new ArrayList<>();

        String sql = "SELECT numero_caratula FROM caratulas_generadas, solicitudes_clientes, documentos_de_solicitudes "
                + "WHERE caratulas_generadas.id_solicitud_cliente_FK = solicitudes_clientes.id_solicitud_cliente "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = solicitudes_clientes.rut_usuario_cotizador_cbrls_FK "
                + "AND solicitudes_clientes.id_solicitud_cliente = documentos_de_solicitudes.id_solicitud_cliente_FK "
                + "AND solicitudes_clientes.rut_usuario_cotizador_cbrls_FK = documentos_de_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND documentos_de_solicitudes.has_repertorio IN (" + Constantes.HAS_REPERTORIO_NO_SABE + ", " + Constantes.HAS_REPERTORIO_SI + ") "
                + "AND caratulas_generadas.fecha_generacion_caratula BETWEEN ? AND ? "
                + "GROUP BY numero_caratula";

        String sqlReingresadas = "SELECT numero_caratula FROM caratulas_generadas, caratulas_reingresadas, solicitudes_clientes, documentos_de_solicitudes "
                + "WHERE caratulas_generadas.id_solicitud_cliente_FK = solicitudes_clientes.id_solicitud_cliente "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = solicitudes_clientes.rut_usuario_cotizador_cbrls_FK "
                + "AND caratulas_generadas.numero_caratula = caratulas_reingresadas.numero_caratula_FK "
                + "AND solicitudes_clientes.id_solicitud_cliente = documentos_de_solicitudes.id_solicitud_cliente_FK "
                + "AND solicitudes_clientes.rut_usuario_cotizador_cbrls_FK = documentos_de_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND documentos_de_solicitudes.has_repertorio IN (" + Constantes.HAS_REPERTORIO_NO_SABE + ", " + Constantes.HAS_REPERTORIO_SI + ") "
                + "AND caratulas_reingresadas.fecha_reingreso BETWEEN ? AND ? "
                + "GROUP BY numero_caratula";

        String sqlConRepertorioHoy = "SELECT numero_caratula FROM caratulas_generadas, repertorios_documentos_solicitudes, documentos_de_solicitudes "
                + "WHERE caratulas_generadas.id_solicitud_cliente_FK = repertorios_documentos_solicitudes.id_solicitud_cliente_FK "
                + "AND caratulas_generadas.id_solicitud_cliente_FK = documentos_de_solicitudes.id_solicitud_cliente_FK "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = repertorios_documentos_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = documentos_de_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND repertorios_documentos_solicitudes.id_solicitud_cliente_FK = documentos_de_solicitudes.id_solicitud_cliente_FK "
                + "AND repertorios_documentos_solicitudes.rut_usuario_cotizador_cbrls_FK = documentos_de_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = repertorios_documentos_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND documentos_de_solicitudes.has_repertorio IN (" + Constantes.HAS_REPERTORIO_NO_SABE + ", " + Constantes.HAS_REPERTORIO_SI + ") "
                + "AND fecha_repertorio BETWEEN ? AND ? GROUP BY numero_caratula";

        ArrayList<Long> caratulas_encontradas = new ArrayList<>();

        Class.forName(ConectorMySql.JDBC_DRIVER);

        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS);
                PreparedStatement prp = conn.prepareStatement(sql);
                PreparedStatement prpReingresadas = conn.prepareStatement(sqlReingresadas);
                PreparedStatement prp3 = conn.prepareStatement(sqlConRepertorioHoy);) {
            prp.setLong(1, date_long);
            prp.setLong(2, date_long + Constantes.UN_DIA_MILLIS);

            prpReingresadas.setLong(1, date_long);
            prpReingresadas.setLong(2, date_long + Constantes.UN_DIA_MILLIS);

            prp3.setLong(1, date_long);
            prp3.setLong(2, date_long + Constantes.UN_DIA_MILLIS);

            System.out.println("prp: " + prp.toString());
            try (ResultSet rs = prp.executeQuery();
                    ResultSet rs2 = prpReingresadas.executeQuery();
                    ResultSet rs3 = prp3.executeQuery()) {
                while (rs.next()) {
                    caratulas_encontradas.add(rs.getLong("numero_caratula"));
                }

                while (rs2.next()) {
                    if (!caratulas_encontradas.contains(rs2.getLong("numero_caratula"))) {
                        CaratulaRepertorio car = getCaratulaLibroRepertorio(rs2.getLong("numero_caratula"));

                        String evaluarRepertorios = "SELECT fecha_repertorio FROM repertorios_documentos_solicitudes WHERE id_solicitud_cliente_FK = ? AND rut_usuario_cotizador_cbrls_FK = ? ORDER BY fecha_repertorio DESC LIMIT 1 ";

                        try (PreparedStatement prpEv = conn.prepareStatement(evaluarRepertorios)) {
                            prpEv.setLong(1, car.getId_solicitud_cliente());
                            prpEv.setInt(2, car.getRut_usuario_cotizador_cbrls());

                            try (ResultSet rsEv = prpEv.executeQuery()) {
                                if (rsEv.first()) {
                                    if (Funciones.isRepertorioVencido(rsEv.getLong("fecha_repertorio"))) {
                                        caratulas.add(car);
                                        caratulas_encontradas.add(car.getNumero_caratula());
                                    }
                                } else {
                                    caratulas.add(car);
                                }
                            }
                        }
                    }
                }

                while (rs3.next()) {
                    long pivot = rs3.getLong("numero_caratula");
                    if (!caratulas_encontradas.contains(pivot)) {
                        caratulas_encontradas.add(pivot);
                    }
                }
            }
        }

        for (long n : caratulas_encontradas) {
            caratulas.add(getCaratulaLibroRepertorio(n));
        }

        return caratulas;
    }

    public static CaratulaRepertorio getCaratulaLibroRepertorio(long numero_caratula) throws SQLException, ClassNotFoundException {

        CaratulaRepertorio c = null;

        String sql = "SELECT * FROM caratulas_generadas, solicitudes_clientes, clientes "
                + "WHERE id_solicitud_cliente_FK = id_solicitud_cliente "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = solicitudes_clientes.rut_usuario_cotizador_cbrls_FK "
                + "AND rut_cliente_FK = rut_cliente "
                + "AND numero_caratula = ?";

        Class.forName(ConectorMySql.JDBC_DRIVER);

        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {
            prp.setLong(1, numero_caratula);

            try (ResultSet rs = prp.executeQuery()) {
                if (rs.first()) {
                    c = new CaratulaRepertorio();
                    c.setId_solicitud_cliente(rs.getLong("caratulas_generadas.id_solicitud_cliente_FK"));
                    c.setNumero_caratula(numero_caratula);
                    c.setRut_usuario_cotizador_cbrls(rs.getInt("caratulas_generadas.rut_usuario_cotizador_cbrls_FK"));
                    c.setRut_cliente(rs.getInt("rut_cliente"));
                    c.setNombre_cliente(rs.getString("nombre_completo"));
                    c.setFecha_generacion_caratula(rs.getLong("fecha_generacion_caratula"));
                    c.setFecha_reingreso(getFechaReingresoCaratula(c.getNumero_caratula()));
                    c.setDocumentos(getDocumentosCaratulaLibroRepertorio(c.getId_solicitud_cliente(), c.getRut_usuario_cotizador_cbrls()));
                    c.setObservaciones_repertorio(getObservacionesRepertorio(c.getNumero_caratula()));
                }
            }
        }
        return c;

    }

    private static long getFechaReingresoCaratula(long numero_caratula) throws SQLException, ClassNotFoundException {
        String sql = "SELECT fecha_reingreso FROM caratulas_reingresadas WHERE numero_caratula_FK = ? ORDER BY fecha_reingreso DESC LIMIT 1";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {

            prp.setLong(1, numero_caratula);

            try (ResultSet rs = prp.executeQuery()) {
                if (rs.first()) {
                    return rs.getLong("fecha_reingreso");
                } else {
                    return 0;
                }
            }
        }
    }

    private static ArrayList<DocumentoLibroRepertorio> getDocumentosCaratulaLibroRepertorio(long id_solicitud_cliente, int rut_usuario_cotizador_cbrls) throws SQLException, ClassNotFoundException {

        ArrayList<DocumentoLibroRepertorio> documentos = new ArrayList<>();
        String sql = "SELECT * FROM documentos_de_solicitudes, tipos_certificados WHERE id_tipo_certificado_FK = id_tipo_certificado AND "
                + "id_solicitud_cliente_FK = ? AND rut_usuario_cotizador_cbrls_FK = ? ORDER BY orden_repertorio";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql);) {
            prp.setLong(1, id_solicitud_cliente);
            prp.setInt(2, rut_usuario_cotizador_cbrls);

            try (ResultSet rs = prp.executeQuery()) {
                while (rs.next()) {
                    DocumentoLibroRepertorio d = new DocumentoLibroRepertorio();
                    d.setTipo_certificado(rs.getString("tipo_certificado"));
                    d.setHas_repertorio(rs.getInt("has_repertorio"));
                    d.setId_solicitud_cliente(rs.getLong("id_solicitud_cliente_FK"));
                    d.setId_tipo_certificado(rs.getInt("id_tipo_certificado_FK"));
                    d.setId_tipo_registro(rs.getInt("id_tipo_registro_FK"));
                    d.setNumero_carillas(rs.getInt("numero_carillas"));
                    d.setNumero_copias(rs.getInt("numero_copias"));
                    d.setRut_usuario_cotizador_cbrls(rut_usuario_cotizador_cbrls);
                    d.setPrecio_unitario(rs.getInt("precio_unitario"));
                    d.setOrden_repertorio(rs.getInt("orden_repertorio"));
                    d.setIndice(rs.getString("indice"));
                    d.setRepertorios_documento(getRepertoriosDocumento(d));

                    documentos.add(d);
                }
            }
        }

        return documentos;
    }

    public static ArrayList<RepertorioDocumento> getRepertoriosDocumento(DocumentoLibroRepertorio d) throws SQLException, ClassNotFoundException {
        ArrayList<RepertorioDocumento> repertorios = new ArrayList<>();

        String sql = "SELECT * FROM repertorios_documentos_solicitudes "
                + "WHERE id_solicitud_cliente_FK = ? AND rut_usuario_cotizador_cbrls_FK = ? AND id_tipo_registro_FK = ? "
                + "AND id_tipo_certificado_FK = ? AND indice_FK = ? AND precio_unitario_FK = ? ORDER BY orden ASC";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {
            prp.setLong(1, d.getId_solicitud_cliente());
            prp.setInt(2, d.getRut_usuario_cotizador_cbrls());
            prp.setInt(3, d.getId_tipo_registro());
            prp.setInt(4, d.getId_tipo_certificado());
            prp.setString(5, d.getIndice());
            prp.setInt(6, d.getPrecio_unitario());
            String getDataNombres = "SELECT * FROM clases, registros, materias WHERE id_clase = ? AND id_registro = ? AND id_materia = ?";

            try (ResultSet rs = prp.executeQuery()) {
                while (rs.next()) {
                    System.out.println("Encontró xd");
                    RepertorioDocumento r = new RepertorioDocumento();
                    r.setN_repertorio(rs.getInt("n_repertorio"));
                    r.setFecha_repertorio(rs.getLong("fecha_repertorio"));
                    r.setId_libro_repertorios(rs.getLong("id_libro_repertorios_FK"));
                    r.setId_clase(rs.getInt("id_clase_FK"));
                    r.setId_materia(rs.getInt("id_materia_FK"));
                    r.setId_registro(rs.getInt("id_registro_FK"));
                    r.setOrden(rs.getInt("orden"));
                    r.setIndice_anho(rs.getInt("indice_anho"));
                    r.setIndice_numero(rs.getString("indice_numero"));
                    r.setIndice_foja(rs.getString("indice_foja"));
                    r.setContratantes_documento_repertorio(getContratantesDocumentoRepertorio(d, r));
                    repertorios.add(r);

                    try (PreparedStatement prp2 = conn.prepareStatement(getDataNombres)) {
                        prp2.setInt(1, r.getId_clase());
                        prp2.setInt(2, r.getId_registro());
                        prp2.setInt(3, r.getId_materia());

                        try (ResultSet rs2 = prp2.executeQuery()) {
                            if (rs2.first()) {
                                r.setClase(rs2.getString("clase"));
                                r.setRegistro(rs2.getString("registro"));
                                r.setMateria(rs2.getString("materia"));
                            }
                        }
                    }

                }

            }

            if (repertorios.isEmpty()) {
                System.out.println("Caratula " + d.getId_solicitud_cliente() + "No tenia repertorios");
                for (int count = 0; count < d.getNumero_copias(); count++) {
                    RepertorioDocumento r = new RepertorioDocumento();
                    r.setN_repertorio(-1);
                    r.setFecha_repertorio(0);
                    r.setId_libro_repertorios(-1);
                    r.setId_clase(1);
                    r.setClase("No definido");
                    r.setId_materia(1);
                    r.setMateria("No definido");
                    r.setId_registro(1);
                    r.setRegistro("No definido");
                    r.setOrden(count);
                    r.setIndice_anho(Calendar.getInstance().get(Calendar.YEAR));
                    r.setIndice_numero("");
                    r.setIndice_foja("");
                    r.setContratantes_documento_repertorio(new ArrayList<ContratanteDocumentoRepertorio>());
                    repertorios.add(r);

                }
            }

        }

        return repertorios;

    }

    private static ArrayList<ContratanteDocumentoRepertorio> getContratantesDocumentoRepertorio(DocumentoLibroRepertorio d, RepertorioDocumento r) throws SQLException, ClassNotFoundException {

        ArrayList<ContratanteDocumentoRepertorio> contratantes = new ArrayList<>();

        String sql = "SELECT * FROM contratantes_de_documentos_repertorios "
                + "WHERE id_solicitud_cliente_FK = ? "
                + "AND rut_usuario_cotizador_cbrls_FK = ? "
                + "AND id_tipo_registro_FK = ? "
                + "AND id_tipo_certificado_FK = ? "
                + "AND indice_FK = ? "
                + "AND precio_unitario_FK = ? "
                + "AND orden_FK = ?";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {
            prp.setLong(1, d.getId_solicitud_cliente());
            prp.setInt(2, d.getRut_usuario_cotizador_cbrls());
            prp.setInt(3, d.getId_tipo_registro());
            prp.setInt(4, d.getId_tipo_certificado());
            prp.setString(5, d.getIndice());
            prp.setInt(6, d.getPrecio_unitario());
            prp.setInt(7, r.getOrden());

            try (ResultSet rs = prp.executeQuery()) {
                while (rs.next()) {
                    ContratanteDocumentoRepertorio c = new ContratanteDocumentoRepertorio();
                    c.setId_contratante(rs.getLong("id_contratante"));
                    c.setNombre_contratante(rs.getString("nombre_contratante"));
                    c.setId_solicitud_cliente(rs.getLong("id_solicitud_cliente_FK"));
                    c.setRut_usuario_cotizador_cbrls(rs.getInt("rut_usuario_cotizador_cbrls_FK"));
                    c.setId_tipo_registro(rs.getInt("id_tipo_registro_FK"));
                    c.setId_tipo_certificado(rs.getInt("id_tipo_certificado_FK"));
                    c.setIndice(rs.getString("indice_FK"));
                    c.setPrecio_unitario(rs.getInt("precio_unitario_FK"));
                    c.setOrden(rs.getInt("orden_FK"));
                    contratantes.add(c);
                }
            }
        }

        return contratantes;
    }

    public static DataLibroRepertorio getDataLibroRepertorio() throws SQLException, ClassNotFoundException {
        String sql1 = "SELECT * FROM materias";
        String sql2 = "SELECT * FROM clases";
        String sql3 = "SELECT * FROM registros";
        ArrayList<JsonObject> materias = new ArrayList<>();
        ArrayList<JsonObject> clases = new ArrayList<>();
        ArrayList<JsonObject> registros = new ArrayList<>();
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp1 = conn.prepareStatement(sql1); PreparedStatement prp2 = conn.prepareStatement(sql2); PreparedStatement prp3 = conn.prepareStatement(sql3)) {

            try (ResultSet rs1 = prp1.executeQuery(); ResultSet rs2 = prp2.executeQuery(); ResultSet rs3 = prp3.executeQuery()) {
                while (rs1.next()) {
                    JsonObject j1 = new JsonObject();

                    j1.addProperty("id_materia", rs1.getInt("id_materia"));
                    j1.addProperty("materia", rs1.getString("materia"));
                    materias.add(j1);
                }

                while (rs2.next()) {
                    JsonObject j2 = new JsonObject();
                    j2.addProperty("id_clase", rs2.getInt("id_clase"));
                    j2.addProperty("clase", rs2.getString("clase"));
                    clases.add(j2);
                }

                while (rs3.next()) {
                    JsonObject j3 = new JsonObject();
                    j3.addProperty("id_registro", rs3.getInt("id_registro"));
                    j3.addProperty("registro", rs3.getString("registro"));
                    registros.add(j3);
                }
            }

        }

        DataLibroRepertorio obj = new DataLibroRepertorio();
        obj.setClases(clases);
        obj.setMaterias(materias);
        obj.setRegistros(registros);

        return obj;

    }

    private static ArrayList<JsonObject> getObservacionesRepertorio(long numero_caratula) throws SQLException, ClassNotFoundException {

        ArrayList<JsonObject> observaciones = new ArrayList<>();
        Class.forName(ConectorMySql.JDBC_DRIVER);
        String sql = "SELECT * FROM observaciones_caratulas_repertorio WHERE numero_caratula_FK = ?";
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {

            prp.setLong(1, numero_caratula);

            try (ResultSet rs = prp.executeQuery()) {
                while (rs.next()) {
                    JsonObject j = new JsonObject();

                    j.addProperty("id_observacion", rs.getLong("id_observacion"));
                    j.addProperty("observacion", rs.getString("observacion"));
                    observaciones.add(j);
                }
            }

        }

        return observaciones;
    }

    public static ArrayList<CaratulaRepertorio> getCaratulasLibroRepertoriosRangoFechaRepertorio(long fecha_inicio, long fecha_fin) throws SQLException, ClassNotFoundException {
        String sql = "SELECT numero_caratula FROM caratulas_generadas, solicitudes_clientes, documentos_de_solicitudes "
                + "WHERE caratulas_generadas.id_solicitud_cliente_FK = solicitudes_clientes.id_solicitud_cliente "
                + "AND caratulas_generadas.rut_usuario_cotizador_cbrls_FK = solicitudes_clientes.rut_usuario_cotizador_cbrls_FK "
                + "AND solicitudes_clientes.id_solicitud_cliente = documentos_de_solicitudes.id_solicitud_cliente_FK "
                + "AND solicitudes_clientes.rut_usuario_cotizador_cbrls_FK = documentos_de_solicitudes.rut_usuario_cotizador_cbrls_FK "
                + "AND documentos_de_solicitudes.fecha_repertorio BETWEEN ? AND ? "
                + "GROUP BY numero_caratula";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        ArrayList<CaratulaRepertorio> caratulas = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sql)) {
            Date date = new Date(fecha_inicio);

            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);

            date = calendar.getTime();
            long inicio = date.getTime();
            System.out.println(Constantes.UN_DIA_MILLIS);

            date = new Date(fecha_inicio);

            calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.set(Calendar.MILLISECOND, 0);
            calendar.set(Calendar.SECOND, 0);
            calendar.set(Calendar.MINUTE, 0);
            calendar.set(Calendar.HOUR_OF_DAY, 0);

            date = calendar.getTime();
            long fin = date.getTime() + Constantes.UN_DIA_MILLIS;

            prp.setLong(1, inicio);
            prp.setLong(2, fin);

            try (ResultSet rs = prp.executeQuery()) {
                while (rs.next()) {
                    caratulas.add(getCaratulaLibroRepertorio(rs.getLong("numero_caratula")));
                }
            }
        }

        return caratulas;
    }

    public static long getUltimoNumeroRepertorio() throws SQLException, ClassNotFoundException {

        String sqlRepertorio = "SELECT * FROM libros_repertorio ORDER BY fecha_libro_repertorios DESC LIMIT 1";
        Class.forName(ConectorMySql.JDBC_DRIVER);
        try (Connection conn = DriverManager.getConnection(ConectorMySql.DB_URL, ConectorMySql.USER, ConectorMySql.PASS); PreparedStatement prp = conn.prepareStatement(sqlRepertorio); ResultSet rs = prp.executeQuery()) {
            if (rs.first()) {
                Calendar anterior = Calendar.getInstance();
                anterior.setTimeInMillis(rs.getLong("fecha_libro_repertorios"));

                Calendar fecha_actual = Calendar.getInstance();

                if (anterior.get(Calendar.YEAR) == fecha_actual.get(Calendar.YEAR)) {
                    String sql = "SELECT n_repertorio "
                            + "FROM repertorios_documentos_solicitudes "
                            + "WHERE id_libro_repertorios_FK = ? ORDER BY n_repertorio DESC LIMIT 1";

                    try (PreparedStatement prp2 = conn.prepareStatement(sql)) {
                        prp2.setLong(1, rs.getLong("id_libro_repertorios"));
                        try (ResultSet rs2 = prp2.executeQuery()) {
                            if (rs2.first()) {
                                return rs2.getLong("n_repertorio");
                            } else {
                                return 0;
                            }
                        }
                    }
                } else {
                    System.out.println("Ya cambió el año, se iniciará un nuevo libro de repertorios");
                    return 0;
                }
            } else {
                return 0;
            }
        }
    }

}
