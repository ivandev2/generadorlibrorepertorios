/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

/**
 *
 */
public abstract class Constantes {

    public static long fecha_generacion_gp = -1;

    public static long timeOfDeploy = System.currentTimeMillis();
    //Constantes para Front-End
    //public static String rootPath = "/CBRoK/";
    //public static String rootPath = "";
    public static String tituloWebpage = "Plataforma Interna CBRoK";
    public static String colorDefault = "blue-grey";

    public static String documentPath = "C://repository";

    public static final long UN_DIA_MILLIS = 86399999;

    public static final int CORRECCION_NO_NECESARIA = -1;
    public static final int CORRECCION_PENDIENTE = 0;
    public static final int CORRECCION_EFECTUADA = 1;
    public static final int CORRECCION_RECHAZADA = 2;

    public static final int HAS_REPERTORIO_NO_SABE = -1;
    public static final int HAS_REPERTORIO_NO = 0;
    public static final int HAS_REPERTORIO_SI = 1;

}
