/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author ivana
 */
public class Funciones {
    
    public static String getDay(int day){
        switch (day) {
            case 1:
                return "Domingo";
            case 2: return "Lunes";
            case 3: return "Martes";
            case 4: return "Miercoles";
            case 5: return "Jueves";
            case 6: return "Viernes";
            case 7: return "Sábado";
            default:
                throw new AssertionError();
        }
    }
    
    
    public static boolean isRepertorioVencido(long fecha_generacion_caratula_long) throws ParseException {

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

        Date fecha_generacion_caratula = new Date(fecha_generacion_caratula_long);

        //FECHA GENERACIÓN CARÁTULA
        Calendar calGeneracion = Calendar.getInstance();
        calGeneracion.setTime(fecha_generacion_caratula);
        String fechaGeneracion = sdf.format(calGeneracion.getTime());

        //FECHA CON REPERTORIO VENCIDO
        Calendar calRepertorio = calGeneracion;
        calRepertorio.add(Calendar.MONTH, 2);
        calRepertorio.add(Calendar.DAY_OF_MONTH, 1);
        String fechaRepertorio = sdf.format(calRepertorio.getTime());
        Date fechaRepertorioDate = sdf.parse(fechaRepertorio);

        //FECHA HOY
        Calendar calHoy = Calendar.getInstance();
        String fechaHoy = sdf.format(calHoy.getTime());
        Date fechaHoyDate = sdf.parse(fechaHoy);

        if (fechaHoyDate.getTime() >= fechaRepertorioDate.getTime()) {
            return true;
        } else {
            return false;
        }

    }

    
}
