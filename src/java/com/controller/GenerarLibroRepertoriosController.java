/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.estructuras.CaratulaRepertorio;
import com.estructuras.CustomHttpResponse;
import com.estructuras.DocumentoLibroRepertorio;
import com.estructuras.RepertorioDocumento;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.itextpdf.text.BaseColor;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.utils.Constantes;
import com.utils.Funciones;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.servlet.http.HttpServletResponse;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 *
 * @author ivana
 */
public class GenerarLibroRepertoriosController {

    public static void generarPdfLibroRepertorios(HttpServletResponse response, CaratulaRepertorio[] ca) throws SQLException, ClassNotFoundException, DocumentException, FileNotFoundException, IOException, ParseException {

        ArrayList<CaratulaRepertorio> caratulas = getCaratulasLibroRepertoriosApi();

        for(CaratulaRepertorio c : caratulas){
            System.out.println("Caratula encontrada: " + c.getNumero_caratula());
        }
        
        PdfPTable tabla = new PdfPTable(11);
        float base = 18;
        float px = 8.8f;
        float[] rowSizes = {
            8 * px + base, //Caratula
            6 * px + base, //Numero
            5 * px + base, //Fecha
            10 * px + base, //Requirente
            7 * px + base, //Materia
            15 * px + base, //Contratante (1)
            15 * px + base, //Contratante (2)
            15 * px + base, //Contratante (3)
            5 * px + base, //Clase
            8 * px + base, //Registro
            4 * px + base, //Hora
        };

        int totalSize = 0;

        tabla.setLockedWidth(true);

        PdfPCell pcell = new PdfPCell(new Phrase(""));
        Font boldFont = new Font(Font.FontFamily.HELVETICA, 12, Font.BOLD);

        pcell.setHorizontalAlignment(Element.ALIGN_CENTER);

        pcell.setPhrase(new Phrase("CARÁTULA", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("NÚMERO", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("FECHA", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("REQUIRENTE", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("MATERIA", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("CONTRATANTE (1)", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("CONTRATANTE (2)", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("CONTRATANTE (3)", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("CLASE", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("REGISTRO", boldFont));
        tabla.addCell(pcell);

        pcell.setPhrase(new Phrase("HORA", boldFont));
        tabla.addCell(pcell);

        ArrayList<JsonObject> data = new ArrayList<>();
        int registros = 0;
        String ultimo_repertorio = "";

        for (CaratulaRepertorio c : caratulas) {
            Calendar cal = GregorianCalendar.getInstance();
            cal.setTimeInMillis(c.getFecha_generacion_caratula());
            String numero_caratula = c.getNumero_caratula() + "";
            String fecha = (cal.get(Calendar.DATE) < 10 ? "0" + cal.get(Calendar.DATE) : cal.get(Calendar.DATE)) + "/" + (cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : cal.get(Calendar.MONTH) + 1);
            String requirente = c.getNombre_cliente();

            for (DocumentoLibroRepertorio doc : c.getDocumentos()) {

                for (RepertorioDocumento rep : doc.getRepertorios_documento()) {
                    if (doc.getHas_repertorio() != Constantes.HAS_REPERTORIO_SI) {
                        continue;
                    }

                    if (rep.getN_repertorio() == -1) {
                        continue;
                    }
                    
                    System.out.println("Econtro un has repertorio si");
                    String contratante1 = "";
                    String contratante2 = "";
                    String contratante3 = "";
                    if (rep.getContratantes_documento_repertorio().size() > 0) {
                        contratante1 = rep.getContratantes_documento_repertorio().get(0).getNombre_contratante();
                        if (rep.getContratantes_documento_repertorio().size() > 1) {
                            contratante2 = rep.getContratantes_documento_repertorio().get(1).getNombre_contratante();
                            if (rep.getContratantes_documento_repertorio().size() > 2) {
                                contratante3 = rep.getContratantes_documento_repertorio().get(2).getNombre_contratante();
                            }
                        }
                    }

                    String materia = rep.getMateria();
                    String clase = rep.getClase();
                    String registro = rep.getRegistro();
                    String hora = cal.get(Calendar.HOUR_OF_DAY) + ":" + cal.get(Calendar.MINUTE);
                    String numero = rep.getN_repertorio() + "";

                    JsonObject j = new JsonObject();
                    j.addProperty("numero_caratula", numero_caratula);
                    j.addProperty("numero", numero);
                    j.addProperty("fecha", fecha);
                    j.addProperty("requirente", requirente);
                    j.addProperty("materia", materia);
                    j.addProperty("contratante1", contratante1);
                    j.addProperty("contratante2", contratante2);
                    j.addProperty("contratante3", contratante3);
                    j.addProperty("clase", clase);
                    j.addProperty("registro", registro);
                    j.addProperty("hora", hora);
                    data.add(j);

                    pcell.setHorizontalAlignment(Element.ALIGN_CENTER);

                    pcell.setPhrase(new Phrase(numero_caratula.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(numero.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(fecha.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(requirente.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(materia.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(contratante1.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(contratante2.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(contratante3.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(clase.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(registro.toUpperCase()));
                    tabla.addCell(pcell);

                    pcell.setPhrase(new Phrase(hora.toUpperCase()));
                    tabla.addCell(pcell);

                    rowSizes[0] = numero_caratula.length() * px + base > rowSizes[0] ? numero_caratula.length() * px + base : rowSizes[0];
                    rowSizes[1] = numero.length() * px + base > rowSizes[1] ? numero.length() * px + base : rowSizes[1];
                    rowSizes[2] = fecha.length() * px + base > rowSizes[2] ? fecha.length() * px + base : rowSizes[2];
                    rowSizes[3] = requirente.length() * px + base > rowSizes[3] ? requirente.length() * px + base : rowSizes[3];
                    rowSizes[4] = materia.length() * px + base > rowSizes[4] ? materia.length() * px + base : rowSizes[4];
                    rowSizes[5] = contratante1.length() * px + base > rowSizes[5] ? contratante1.length() * px + base : rowSizes[5];
                    rowSizes[6] = contratante2.length() * px + base > rowSizes[6] ? contratante2.length() * px + base : rowSizes[6];
                    rowSizes[7] = contratante3.length() * px + base > rowSizes[7] ? contratante3.length() * px + base : rowSizes[7];
                    rowSizes[8] = clase.length() * px + base > rowSizes[8] ? clase.length() * px + base : rowSizes[8];
                    rowSizes[9] = registro.length() * px + base > rowSizes[9] ? registro.length() * px + base : rowSizes[9];
                    rowSizes[10] = hora.length() * px + base > rowSizes[10] ? hora.length() * px + base : rowSizes[10];

                    registros++;

                    ultimo_repertorio = numero;
                }
            }

        }

        for (float r : rowSizes) {
            totalSize += r;
        }

        tabla.setTotalWidth(totalSize);
        tabla.setWidths(rowSizes);
        Float h = tabla.getTotalHeight() + 250;

        Rectangle r = new Rectangle(totalSize, h);
        Document documento = new Document(r, 0, 0, 0, 0);
        new File(getPathLibroRepertoriosHoy()).mkdirs();
        PdfWriter writer = PdfWriter.getInstance(documento, new FileOutputStream(getPathLibroRepertoriosHoy() + "/libro_repertorios.pdf"));
        documento.open();
        Paragraph p1 = new Paragraph("Libro de repertorios emitido con fecha x", boldFont);
        p1.setIndentationLeft(30f);
        p1.setSpacingAfter(10);
        documento.add(p1);

        documento.add(tabla);
        Calendar hoy = Calendar.getInstance();
        //----------------
        Paragraph p = new Paragraph("CERTIFICO: QUE HOY " + Funciones.getDay(hoy.get(Calendar.DAY_OF_WEEK)).toUpperCase() + " " + hoy.get(Calendar.DATE) + " DE " + (hoy.get(Calendar.MONTH) + 1) + " DE " + hoy.get(Calendar.YEAR) + " SE PRACTICARON " + registros + " ANOTACIONES HASTA LA NÚMERO " + ultimo_repertorio + ". ", boldFont);
        p.setIndentationLeft(30f);
        documento.add(p);
        Paragraph p2 = new Paragraph("Este documento incorpora una firma electrónica avanzada, según lo indicado en la Ley N°19.799 y el Autoacordado de la Excema. Corte Suprema. Su validez puede ser consultada en el sitio Web www.cbrls.cl con el código de verificación indicado sobre estas líneas");
        p2.setAlignment(Element.ALIGN_JUSTIFIED_ALL);
        PdfContentByte canvas = writer.getDirectContent();

        Rectangle rect = new Rectangle(80, 10, 450, 100);
        rect.setBorder(Rectangle.BOX);
        rect.setBorderWidth(1);

        rect.setBorderColor(BaseColor.WHITE);
        canvas.rectangle(rect);

        ColumnText ct = new ColumnText(canvas);
        ct.setSimpleColumn(rect);
        ct.addElement(p2);
        ct.go();
        canvas.rectangle(rect);
        documento.add(rect);

        documento.close();

        File pdfFile;
        pdfFile = new File(getPathLibroRepertoriosHoy() + "/libro_repertorios.pdf");
        try (FileInputStream fileInputStream = new FileInputStream(pdfFile); OutputStream responseOutputStream = response.getOutputStream();) {

            int bytes;

            response.setContentType("application/pdf;charset=UTF-8");
            response.addHeader("Content-Disposition", "inline; filename=libro_repertorios.pdf");
            response.setContentLength((int) pdfFile.length());

            while ((bytes = fileInputStream.read()) != -1) {
                responseOutputStream.write(bytes);
            }
        }
    }

    public static String getPathLibroRepertoriosHoy() {
        Calendar cal = GregorianCalendar.getInstance();

        return Constantes.documentPath + "/libro_repertorios/ " + (cal.get(Calendar.DATE) < 10 ? "0" + cal.get(Calendar.DATE) : cal.get(Calendar.DATE)) + "-" + (cal.get(Calendar.MONTH) + 1 < 10 ? "0" + (cal.get(Calendar.MONTH) + 1) : cal.get(Calendar.MONTH) + 1) + "-" + cal.get(Calendar.YEAR);

    }

    public static void main(String[] args) {
        try {
            getCaratulasLibroRepertoriosApi();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

    }

    private static ArrayList<CaratulaRepertorio> getCaratulasLibroRepertoriosApi() throws MalformedURLException, IOException {

        String rawData = "";
        String url = "http://localhost:8080/PlataformaInternaCBRLS/api/librorepertorios?action=get-caratulas-libro-repertorios";
        URL obj = new URL(url);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();

        //add reuqest header
        con.setRequestMethod("POST"); //e.g POST
        con.setRequestProperty("Accept", "application/json"); //e.g key = Accept, value = application/json

        con.setDoOutput(true);

        OutputStreamWriter w = new OutputStreamWriter(con.getOutputStream(), "UTF-8");

        w.write(rawData);
        w.close();

        int responseCode = con.getResponseCode();

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));

        String inputLine;
        StringBuilder response = new StringBuilder();

        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }

        in.close();
        System.out.println("Buffered reader : " + response.toString());
        System.out.println("Response code : " + responseCode);
        System.out.println(response.toString());

        CustomHttpResponse respuesta;
        Gson gson = new Gson();
        respuesta = gson.fromJson(response.toString(), CustomHttpResponse.class);

        System.out.println("respuesta parsed: " + respuesta.getObj());

        return respuesta.getObj();
        //Use Jsoup on response to parse it if it makes your work easier.
    }

}
