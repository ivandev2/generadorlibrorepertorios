/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.controller;

import com.estructuras.CaratulaRepertorio;
import com.estructuras.Response;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.itextpdf.text.DocumentException;
import com.test.LibroRepertoriosController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author ivana
 */
@WebServlet(name = "SLibroRepertorios", urlPatterns = {"/api/libro-repertorios"})
public class SLibroRepertorios extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    HttpSession sesion;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */

            Response respuesta;
            Gson gson = new Gson();

            sesion = request.getSession();

            String lista_caratulas = request.getParameter("lista_caratulas");

            actionGenerarLibroRepertorios(out, response, lista_caratulas);


        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void actionGenerarLibroRepertorios(PrintWriter out, HttpServletResponse response, String lista_caratulas) {
        Gson gson = new Gson();
        Response respuesta;

        CaratulaRepertorio[] caratulas = null;
        try {
            caratulas = gson.fromJson(lista_caratulas, CaratulaRepertorio[].class);
        } catch (JsonSyntaxException e) {
            System.out.println("No venia la clase");
        }
//        try {
//            URL url = new URL("http://example.com");
//            HttpURLConnection con = (HttpURLConnection) url.openConnection();
//            con.setRequestMethod("GET");
//
//        } catch (Exception e) {
//            respuesta = new Response(false, "ERROR", e.toString());
//            out.print(gson.toJson(respuesta));
//        }

        try {
            response.reset();
            GenerarLibroRepertoriosController.generarPdfLibroRepertorios(response, caratulas);
            out.flush();
            out.print(response);
            //   OutputStream outStream = response.getOutputStream();
        } catch (DocumentException | IOException | ClassNotFoundException | SQLException | ParseException e) {
            respuesta = new Response(false, "ERROR", e.toString());
            out.print(gson.toJson(respuesta));
        }
    }

}
