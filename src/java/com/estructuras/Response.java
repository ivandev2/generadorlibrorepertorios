package com.estructuras;

import java.util.Date;

/**
     * Clase que sirve para formatear la respuesta desde un servlet hacia el front-end
     *
     * @author Dimitry J. Castex <dimitrycastex@gmail.com>
     */
public class Response
{
     /**
     * status: true si trajo la respuesta esperada y false si trajo un error.
     * code: string que trae un código para mostrar texto user-like en el front-end
     * obj: si status fue true, trae el objeto en cuestión; si status fue false, trae un string del error para los developers.
     * fecha: muestra la fecha de retorno de la información, util para encriptar
     */
    
    private boolean status;
    private String code;
    private Object  obj;
    private final Date fecha;
    
    public Response(boolean status, String code, Object obj)
    {
        this.status = status;
        this.code = code;
        this.obj = obj;
        fecha = new Date();
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Object getObj() {
        return obj;
    }

    public void setObj(Object obj) {
        this.obj = obj;
    }

    public Date getFecha() {
        return fecha;
    }
}
