/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

import java.util.ArrayList;

/**
 *
 * @author ivana
 */
public class RepertorioDocumento {

    long id_libro_repertorios;
    int orden;
    int n_repertorio;
    long fecha_repertorio;

    int id_materia;
    String materia;

    int id_clase;
    String clase;

    int id_registro;
    String registro;

    String indice_foja;
    String indice_numero;
    int indice_anho;

    ArrayList<ContratanteDocumentoRepertorio> contratantes_documento_repertorio;

    public long getId_libro_repertorios() {
        return id_libro_repertorios;
    }

    public void setId_libro_repertorios(long id_libro_repertorios) {
        this.id_libro_repertorios = id_libro_repertorios;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

    public int getN_repertorio() {
        return n_repertorio;
    }

    public void setN_repertorio(int n_repertorio) {
        this.n_repertorio = n_repertorio;
    }

    public long getFecha_repertorio() {
        return fecha_repertorio;
    }

    public void setFecha_repertorio(long fecha_repertorio) {
        this.fecha_repertorio = fecha_repertorio;
    }

    public int getId_materia() {
        return id_materia;
    }

    public void setId_materia(int id_materia) {
        this.id_materia = id_materia;
    }

    public String getMateria() {
        return materia;
    }

    public void setMateria(String materia) {
        this.materia = materia;
    }

    public int getId_clase() {
        return id_clase;
    }

    public void setId_clase(int id_clase) {
        this.id_clase = id_clase;
    }

    public String getClase() {
        return clase;
    }

    public void setClase(String clase) {
        this.clase = clase;
    }

    public int getId_registro() {
        return id_registro;
    }

    public void setId_registro(int id_registro) {
        this.id_registro = id_registro;
    }

    public String getRegistro() {
        return registro;
    }

    public void setRegistro(String registro) {
        this.registro = registro;
    }

    public String getIndice_foja() {
        return indice_foja;
    }

    public void setIndice_foja(String indice_foja) {
        this.indice_foja = indice_foja;
    }

    public String getIndice_numero() {
        return indice_numero;
    }

    public void setIndice_numero(String indice_numero) {
        this.indice_numero = indice_numero;
    }

    public int getIndice_anho() {
        return indice_anho;
    }

    public void setIndice_anho(int indice_anho) {
        this.indice_anho = indice_anho;
    }

    public ArrayList<ContratanteDocumentoRepertorio> getContratantes_documento_repertorio() {
        return contratantes_documento_repertorio;
    }

    public void setContratantes_documento_repertorio(ArrayList<ContratanteDocumentoRepertorio> contratantes_documento_repertorio) {
        this.contratantes_documento_repertorio = contratantes_documento_repertorio;
    }

}
