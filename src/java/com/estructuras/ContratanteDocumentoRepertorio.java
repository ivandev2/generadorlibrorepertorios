/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

/**
 *
 * @author Ivan
 */
public class ContratanteDocumentoRepertorio {

    long id_contratante;
    String nombre_contratante;
    long id_solicitud_cliente;
    int rut_usuario_cotizador_cbrls;
    int id_tipo_registro;
    int id_tipo_certificado;
    String indice;
    int precio_unitario;
    int orden;

    public long getId_contratante() {
        return id_contratante;
    }

    public void setId_contratante(long id_contratante) {
        this.id_contratante = id_contratante;
    }

    public String getNombre_contratante() {
        return nombre_contratante;
    }

    public void setNombre_contratante(String nombre_contratante) {
        this.nombre_contratante = nombre_contratante;
    }

    public long getId_solicitud_cliente() {
        return id_solicitud_cliente;
    }

    public void setId_solicitud_cliente(long id_solicitud_cliente) {
        this.id_solicitud_cliente = id_solicitud_cliente;
    }

    public int getRut_usuario_cotizador_cbrls() {
        return rut_usuario_cotizador_cbrls;
    }

    public void setRut_usuario_cotizador_cbrls(int rut_usuario_cotizador_cbrls) {
        this.rut_usuario_cotizador_cbrls = rut_usuario_cotizador_cbrls;
    }

    public int getId_tipo_registro() {
        return id_tipo_registro;
    }

    public void setId_tipo_registro(int id_tipo_registro) {
        this.id_tipo_registro = id_tipo_registro;
    }

    public int getId_tipo_certificado() {
        return id_tipo_certificado;
    }

    public void setId_tipo_certificado(int id_tipo_certificado) {
        this.id_tipo_certificado = id_tipo_certificado;
    }

    public String getIndice() {
        return indice;
    }

    public void setIndice(String indice) {
        this.indice = indice;
    }

    public int getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(int precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public int getOrden() {
        return orden;
    }

    public void setOrden(int orden) {
        this.orden = orden;
    }

}
