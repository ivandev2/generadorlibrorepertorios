/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

import java.util.ArrayList;

/**
 *
 * @author Ivan
 */
public class DocumentoLibroRepertorio {

    int id_tipo_registro;
    int id_tipo_certificado;
    long id_solicitud_cliente;
    int rut_usuario_cotizador_cbrls;
    String indice;
    int numero_carillas;
    int numero_copias;
    int precio_unitario;
    int has_repertorio;

    int orden_repertorio;

//    public boolean isModificable() {
//        return modificable;
//    }
//
//    public void setModificable(boolean modificable) {
//        this.modificable = modificable;
//    }
    public int getOrden_repertorio() {
        return orden_repertorio;
    }

    public void setOrden_repertorio(int orden_repertorio) {
        this.orden_repertorio = orden_repertorio;
    }

    ArrayList<RepertorioDocumento> repertorios_documento;

    public ArrayList<RepertorioDocumento> getRepertorios_documento() {
        return repertorios_documento;
    }

    public void setRepertorios_documento(ArrayList<RepertorioDocumento> repertorios_documento) {
        this.repertorios_documento = repertorios_documento;
    }

    String tipo_certificado;

    public String getTipo_certificado() {
        return tipo_certificado;
    }

    public void setTipo_certificado(String tipo_certificado) {
        this.tipo_certificado = tipo_certificado;
    }

    public int getId_tipo_registro() {
        return id_tipo_registro;
    }

    public void setId_tipo_registro(int id_tipo_registro) {
        this.id_tipo_registro = id_tipo_registro;
    }

    public int getId_tipo_certificado() {
        return id_tipo_certificado;
    }

    public void setId_tipo_certificado(int id_tipo_certificado) {
        this.id_tipo_certificado = id_tipo_certificado;
    }

    public long getId_solicitud_cliente() {
        return id_solicitud_cliente;
    }

    public void setId_solicitud_cliente(long id_solicitud_cliente) {
        this.id_solicitud_cliente = id_solicitud_cliente;
    }

    public int getRut_usuario_cotizador_cbrls() {
        return rut_usuario_cotizador_cbrls;
    }

    public void setRut_usuario_cotizador_cbrls(int rut_usuario_cotizador_cbrls) {
        this.rut_usuario_cotizador_cbrls = rut_usuario_cotizador_cbrls;
    }

    public String getIndice() {
        return indice;
    }

    public void setIndice(String indice) {
        this.indice = indice;
    }

    public int getNumero_carillas() {
        return numero_carillas;
    }

    public void setNumero_carillas(int numero_carillas) {
        this.numero_carillas = numero_carillas;
    }

    public int getNumero_copias() {
        return numero_copias;
    }

    public void setNumero_copias(int numero_copias) {
        this.numero_copias = numero_copias;
    }

    public int getPrecio_unitario() {
        return precio_unitario;
    }

    public void setPrecio_unitario(int precio_unitario) {
        this.precio_unitario = precio_unitario;
    }

    public int getHas_repertorio() {
        return has_repertorio;
    }

    public void setHas_repertorio(int has_repertorio) {
        this.has_repertorio = has_repertorio;
    }

}
