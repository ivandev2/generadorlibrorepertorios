/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

import com.google.gson.JsonObject;
import java.util.ArrayList;

/**
 *
 * @author Ivan
 */
public class DataLibroRepertorio {

    ArrayList<JsonObject> clases;
    ArrayList<JsonObject> materias;
    ArrayList<JsonObject> registros;

    public ArrayList<JsonObject> getClases() {
        return clases;
    }

    public void setClases(ArrayList<JsonObject> clases) {
        this.clases = clases;
    }

    public ArrayList<JsonObject> getMaterias() {
        return materias;
    }

    public void setMaterias(ArrayList<JsonObject> materias) {
        this.materias = materias;
    }

    public ArrayList<JsonObject> getRegistros() {
        return registros;
    }

    public void setRegistros(ArrayList<JsonObject> registros) {
        this.registros = registros;
    }

}
