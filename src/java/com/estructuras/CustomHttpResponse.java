/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author ivana
 */
public class CustomHttpResponse {

    private boolean status;
    private String code;
    private ArrayList<CaratulaRepertorio> obj;
    private final Date fecha;

    public CustomHttpResponse(Date fecha) {
        this.fecha = fecha;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<CaratulaRepertorio> getObj() {
        return obj;
    }

    public void setObj(ArrayList<CaratulaRepertorio> obj) {
        this.obj = obj;
    }

    public Date getFecha() {
        return fecha;
    }
}
