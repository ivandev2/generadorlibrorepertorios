/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.estructuras;

import com.google.gson.JsonObject;
import java.util.ArrayList;

/**
 *
 * @author Ivan
 */
public class CaratulaRepertorio {

    long numero_caratula;
    long id_solicitud_cliente;
    int rut_usuario_cotizador_cbrls;
    int rut_cliente;

    long fecha_generacion_caratula;
    long fecha_reingreso;

    String nombre_cliente;
    ArrayList<JsonObject> observaciones_repertorio;
    ArrayList<DocumentoLibroRepertorio> documentos;

    public long getFecha_reingreso() {
        return fecha_reingreso;
    }

    public void setFecha_reingreso(long fecha_reingreso) {
        this.fecha_reingreso = fecha_reingreso;
    }

    public ArrayList<JsonObject> getObservaciones_repertorio() {
        return observaciones_repertorio;
    }

    public void setObservaciones_repertorio(ArrayList<JsonObject> observaciones_repertorio) {
        this.observaciones_repertorio = observaciones_repertorio;
    }

    public long getFecha_generacion_caratula() {
        return fecha_generacion_caratula;
    }

    public void setFecha_generacion_caratula(long fecha_generacion_caratula) {
        this.fecha_generacion_caratula = fecha_generacion_caratula;
    }

    public String getNombre_cliente() {
        return nombre_cliente;
    }

    public void setNombre_cliente(String nombre_cliente) {
        this.nombre_cliente = nombre_cliente;
    }

    public long getNumero_caratula() {
        return numero_caratula;
    }

    public void setNumero_caratula(long numero_caratula) {
        this.numero_caratula = numero_caratula;
    }

    public long getId_solicitud_cliente() {
        return id_solicitud_cliente;
    }

    public void setId_solicitud_cliente(long id_solicitud_cliente) {
        this.id_solicitud_cliente = id_solicitud_cliente;
    }

    public int getRut_usuario_cotizador_cbrls() {
        return rut_usuario_cotizador_cbrls;
    }

    public void setRut_usuario_cotizador_cbrls(int rut_usuario_cotizador_cbrls) {
        this.rut_usuario_cotizador_cbrls = rut_usuario_cotizador_cbrls;
    }

    public int getRut_cliente() {
        return rut_cliente;
    }

    public void setRut_cliente(int rut_cliente) {
        this.rut_cliente = rut_cliente;
    }

    public ArrayList<DocumentoLibroRepertorio> getDocumentos() {
        return documentos;
    }

    public void setDocumentos(ArrayList<DocumentoLibroRepertorio> documentos) {
        this.documentos = documentos;
    }

}
